#include <Wire.h>
#include <Adafruit_NeoPixel.h>

#define PIN 6
#define NUMPIXELS 4

void on_receive(void);
void on_request(int);
uint32_t readColor(void);

char buf[60];
uint32_t colors[NUMPIXELS];

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(115200);
  Wire.begin(15);
  Wire.onReceive(on_receive);
  Wire.onRequest(on_request);
  pinMode(LED_BUILTIN, OUTPUT);
  pixels.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  
  delay(100);
}

void on_receive() {
  byte cmd;
  
  pixels.clear();
  Serial.print("Count bytes available: ");
  Serial.println(Wire.available());
  if (Wire.available() != 3 * NUMPIXELS + 1) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    return;  
  }

    cmd = Wire.read();
    for (int i=0; i < NUMPIXELS; i++)
      colors[i] = readColor();
     
    for (int i=0; i < NUMPIXELS; i++)
      pixels.setPixelColor(i, colors[i]);
    
    pixels.show();  
}

void on_request(int bytesCount) {
  
}

uint32_t readColor() {
  int count = 0;
  byte c, r, g, b;
  
  while (Wire.available() && count < 3) {
    switch (count) {
      case 0: 
        r = Wire.read();
        break;
      case 1:
        g = Wire.read();
        break;
      case 2:
        b = Wire.read();
        break;             
      }
      count++;
   }
  return pixels.Color(r, g, b);
}
