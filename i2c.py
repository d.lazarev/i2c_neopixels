#!/usr/bin/env python3
import sys
import os
from pathlib import Path
import ctypes
import struct
import smbus

# Usage: i2c.py rrggbb rrggbb rrggbb rrggbb
# any LED must absent: rrggbb - rrggbb only 1 and 3 led change color.
# - - rrggbb for 3 led only
# Color setting in hex form

def color_in_rgb(col):
    r = int(col[0:2], 16)
    g = int(col[2:4], 16)
    b = int(col[4:6], 16)
    return [r, g, b]

data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
filename = "/var/tmp/i2c_neopixel.sock"
fs = Path(filename)
if fs.is_file() and fs.stat().st_size == 12:
    with fs.open(mode='rb') as fn:
        data = list(fn.read())

print(data)
arg_colors = sys.argv[1:]

while len(arg_colors) < 4:
    arg_colors.append("-")

n_data = []
i = 0
for led_color in arg_colors:
    if led_color == '-':
        rgb = data[i:i+3]
    else:
        rgb = color_in_rgb(led_color)
    n_data += rgb
    i += 3
    print(led_color, rgb)

print(n_data)

bus = smbus.SMBus(1)
address = 0x0f
bus.write_i2c_block_data(address, 0x0f, n_data)
with fs.open(mode='wb+') as fn:
    fn.write(bytearray(n_data))
